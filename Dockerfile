FROM php:apache

MAINTAINER Håkan Hagenrud version: 0.1

COPY index.php /var/www/html/
RUN chown -R www-data.www-data /var/www/html/
